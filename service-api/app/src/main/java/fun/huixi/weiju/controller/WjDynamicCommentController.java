package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.DynamicBusiness;
import fun.huixi.weiju.page.PageData;
import fun.huixi.weiju.pojo.dto.dynamic.PageQueryDynamicCommentDTO;
import fun.huixi.weiju.pojo.dto.dynamic.SaveDynamicCommentDTO;
import fun.huixi.weiju.pojo.vo.dynamic.DynamicCommentItemVO;
import fun.huixi.weiju.service.WjDynamicCommentService;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.*;

import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 动态评论表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjDynamicComment")
public class WjDynamicCommentController extends BaseController {


    @Resource
    private DynamicBusiness dynamicBusiness;

    @Resource
    private WjDynamicCommentService dynamicCommentService;



    
    /**
     *  分页查询动态评论
     * @Author 叶秋 
     * @Date 2021/12/10 10:22
     * @param pageQueryDynamicCommentDTO 查询参数
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("pageQueryDynamicComment")
    public ResultData<PageData<DynamicCommentItemVO>> pageQueryDynamicComment(@RequestBody @Valid PageQueryDynamicCommentDTO pageQueryDynamicCommentDTO){

        Integer userId = CommonUtil.getNowUserId();

        PageData<DynamicCommentItemVO> pageData = dynamicBusiness.pageQueryDynamicComment(userId, pageQueryDynamicCommentDTO);

        return ResultData.ok(pageData);
    }



    /**
     *  保存动态评论
     * @Author 叶秋
     * @Date 2021/11/29 14:43
     * @param saveDynamicCommentDTO 动态评论保存参数
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("saveDynamicComment")
    public ResultData saveDynamicComment(@RequestBody @Valid SaveDynamicCommentDTO saveDynamicCommentDTO){

        dynamicBusiness.saveDynamicComment(saveDynamicCommentDTO);

        return ResultData.ok();
    }



    /**
     *  删除动态评论
     * @Author 叶秋
     * @Date 2021/11/29 14:50
     * @param dynamicCommentId  动态评论id
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @DeleteMapping("removeDynamicComment/{dynamicCommentId}")
    public ResultData removeDynamicComment(@PathVariable Integer dynamicCommentId){

        Integer userId = CommonUtil.getNowUserId();

        // 判断这条评论是否是自己的
        Boolean orMe = dynamicCommentService.judgeCommentOrMe(userId, dynamicCommentId);
        if(!orMe){
            return ResultData.error("该条动态无权删除");
        }

        dynamicBusiness.removeDynamicComment(dynamicCommentId);


        return ResultData.ok();

    }





}


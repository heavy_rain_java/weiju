package fun.huixi.weiju.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

/**
 * 聊天室对应的用户 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjChatUser")
public class WjChatUserController extends BaseController {

}


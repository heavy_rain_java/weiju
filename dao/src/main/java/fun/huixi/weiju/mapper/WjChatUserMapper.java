package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjChatUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 聊天室对应的用户 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjChatUserMapper extends BaseMapper<WjChatUser> {

}

package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjDynamicEndorse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 动态的点赞表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjDynamicEndorseMapper extends BaseMapper<WjDynamicEndorse> {

}

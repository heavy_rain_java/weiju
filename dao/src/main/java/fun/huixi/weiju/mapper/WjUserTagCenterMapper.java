package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjUserTagCenter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与标签的中间表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjUserTagCenterMapper extends BaseMapper<WjUserTagCenter> {

}

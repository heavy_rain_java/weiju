package fun.huixi.weiju.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.huixi.weiju.mapper.WjAppealTagCenterMapper;
import fun.huixi.weiju.pojo.entity.WjAppealTagCenter;
import fun.huixi.weiju.service.WjAppealTagCenterService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求与标志的对应关系 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-10
 */
@Service
public class WjAppealTagCenterServiceImpl extends ServiceImpl<WjAppealTagCenterMapper, WjAppealTagCenter> implements WjAppealTagCenterService {

}

package fun.huixi.weiju.pojo.vo.dynamic;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

/**
 *  评论的信息
 * @Author 叶秋
 * @Date 2021/12/10 10:23
 **/
@Data
public class DynamicCommentItemVO {


    // 评论的用户信息

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像对应的URL地址
     */
    private String headPortrait;


    // 评论的信息

    /**
     * 动态评论id
     */
    private Integer dynamicCommentId;

    /**
     * 对应的动态id
     */
    private Integer dynamicId;


    /**
     * 评论的内容
     */
    private String content;

    /**
     * 评论的点赞数
     */
    private Integer endorseCount;

    /**
     * 评价评论的次数
     */
    private Integer commentCount;


    /**
     * 评论时间
     */
    private LocalDateTime createTime;


}

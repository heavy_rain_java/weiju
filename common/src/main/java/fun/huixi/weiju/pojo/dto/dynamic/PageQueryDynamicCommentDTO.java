package fun.huixi.weiju.pojo.dto.dynamic;

import fun.huixi.weiju.page.PageQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *  分页查询动态的评论
 * @Author 叶秋 
 * @Date 2021/12/10 10:20
 **/
@Data
public class PageQueryDynamicCommentDTO extends PageQuery {

    // 分页参数已经继承

    
    /**
     *  动态id
     **/
    @NotNull(message = "动态id不能为空")
    private Integer dynamicId;

}

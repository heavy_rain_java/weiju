package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态表
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_dynamic")
public class WjDynamic extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 动态id
     */
    @TableId("dynamic_id")
    private Integer dynamicId;

    /**
     * 关联的用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 动态的内容（没有标题之类的）
     */
    @TableField("content")
    private String content;

    /**
     * 动态标签，对应的字典id
     */
    @TableField("tag_dict_id")
    private Integer tagDictId;

    /**
     * 动态所发素材的url地址，多个用逗号隔开
     */
    @TableField("url")
    private String url;

    /**
     * 动态 点赞数||赞同数
     */
    @TableField("endorse_count")
    private Integer endorseCount;

    /**
     * 动态 评论数
     */
    @TableField("comment_count")
    private Integer commentCount;

    /**
     * 动态 浏览量
     */
    @TableField("browse_count")
    private Integer browseCount;



}

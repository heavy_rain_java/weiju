package fun.huixi.weiju.pojo.vo.dynamic;

import lombok.Data;

/**
 * 动态查询DTO
 *
 * @param
 * @Author Zol
 * @Date 2021/11/10
 * @return
 **/
@Data
public class DynamicListVO {

    // 用户信息
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户头像
     */
    private String headPortrait;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 上次在线时间
     */
    private String lastOnlineTime;

    // 动态信息
    /**
     * 动态内容
     */
    private String content;

    /**
     * 动态所发素材的url地址，多个用逗号隔开
     */
    private String url;

    /**
     * 动态 点赞数||赞同数
     */
    private Integer endorseCount;

    /**
     * 动态 评论数
     */
    private Integer commentCount;

    /**
     * 动态 是否点赞
     */
    private Boolean isEndorse;

}

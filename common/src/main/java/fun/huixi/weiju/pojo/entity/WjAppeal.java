package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求表
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal")
public class WjAppeal extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 诉求的id
     */
    @TableId(value = "appeal_id", type = IdType.AUTO)
    private Integer appealId;

    /**
     * 诉求对应的用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 诉求的标题
     */
    @TableField("title")
    private String title;

    /**
     * 诉求的内容
     */
    @TableField("content")
    private String content;

    /**
     * 诉求点赞量
     */
    @TableField("endorse_count")
    private Integer endorseCount;

    /**
     * 诉求的评论量
     */
    @TableField("comment_count")
    private Integer commentCount;

    /**
     * 诉求浏览量
     */
    @TableField("browse_count")
    private Integer browseCount;

    /**
     * 修改次数，需要限定修改次数
     */
    @TableField("update_count")
    private Integer updateCount;

    /**
     * 该动态是否标识了位置信息  0未标识 1已标识
     */
    @TableField("location_auth")
    private Boolean locationAuth;

    /**
     * 纬度
     */
    @TableField("latitude")
    private BigDecimal latitude;

    /**
     * 经度
     */
    @TableField("longitude")
    private BigDecimal longitude;



}

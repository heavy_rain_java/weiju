package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 聊天室（聊天列表）
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_chat")
public class WjChat extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "chat_id", type = IdType.AUTO)
    private Integer chatId;

    /**
     * 诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 聊天室类型 0私聊 1群聊
     */
    @TableField("type")
    private Boolean type;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 限制人数
     */
    @TableField("people_number")
    private Integer peopleNumber;

    /**
     * 聊天室的图片（一对一聊天就用双方的头像组合成一张，一对多就多张组合起来）
     */
    @TableField("url")
    private String url;



}

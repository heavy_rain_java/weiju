package fun.huixi.weiju.pojo.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 存储诉求的地址信息
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal_address")
public class WjAppealAddress extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 诉求具体位置的主键
     */
    @TableId("appeal_address_id")
    private Integer appealAddressId;

    /**
     * 对应的诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 纬度
     */
    @TableField("latitude")
    private BigDecimal latitude;

    /**
     * 经度
     */
    @TableField("longitude")
    private BigDecimal longitude;

    /**
     * 国家
     */
    @TableField("nation")
    private Integer nation;

    /**
     * 省
     */
    @TableField("province")
    private Integer province;

    /**
     * 市
     */
    @TableField("city")
    private Integer city;

    /**
     * 区
     */
    @TableField("district")
    private Integer district;

    /**
     * 行政区划代码
行政区划代码
     */
    @TableField("adcode")
    private Integer adcode;

    /**
     * 详细地址
     */
    @TableField("detailed_address")
    private String detailedAddress;

    /**
     * 备用地址(冗余)
     */
    @TableField("standby_address")
    private String standbyAddress;

    /**
     * 备用的地址code(冗余)
     */
    @TableField("standby_address_code")
    private Integer standbyAddressCode;


}

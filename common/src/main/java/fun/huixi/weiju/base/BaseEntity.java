package fun.huixi.weiju.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fun.huixi.weiju.constant.Constant;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础实体类，所有实体都需要继承
 * @author 叶秋
 */
@Data
public abstract class BaseEntity implements Serializable {

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    public LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     *  状态码 需要根据业务自定义
     */
    @TableField(value = "status_flag", fill = FieldFill.INSERT)
    private Integer statusFlag;

    /**
     *  是否删除
     */
    @JsonIgnore
    @TableLogic(value = Constant.FALSE_STR, delval = Constant.TRUE_STR)
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    private Boolean delFlag;

    /**
     *  乐观锁
     */
    @JsonIgnore
    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;



}
